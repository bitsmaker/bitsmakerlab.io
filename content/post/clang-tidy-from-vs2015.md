+++
date = "2017-04-12T07:59:14-07:00"
draft = false
title = "Clang Tidy from VS2015 using CMake"

+++
In this post I show how to use clang-tidy from within VS2015. The prerequisites are that you must be using [CMake](https://cmake.org) to generate your project. During the install of CMake I checked the option to add CMake to my PATH environment variable. I also installed [Clang](http://releases.llvm.org/download.html) and added it to the PATH environment variable as well. Finally, get [Ninja](https://ninja-build.org/) and put it in some directory. Ninja is needed because when I tried to use _Visual Studio 14 2015 Win64_ for the _G_ option, CMake kept switching the compiler to cl.exe despite setting the compiler in the command-line options. I assume that you are doing *out of source* builds, and that all your source is under the ```src``` directory. An example source tree would be:
```
c:\projects\projectA
|----buildx64_VS2015
|----buildx64_Ninja
|----src
    |----srcA1
    |----srcA2
CMakeLists.txt
```
For clang-tidy to work one has to create the ```compile_commands.json``` file. With CMake it is easy to do. I created a batch file to do just that. Make sure to edit the file where it specifies the architecture to be _amd64_ to meet your needs. 

### Batch file for building compile_commands.json
```gen_compile_commands.bat
@echo off
if [%1]==[] goto usage

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
pushd %1
cmake -DCMAKE_C_COMPILER="clang-cl.exe" -DCMAKE_CXX_COMPILER="clang-cl.exe" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_MAKE_PROGRAM="c:/Program Files/Ninja/ninja.exe" -G "Ninja" ..
popd

@echo Done.
goto :eof

:usage
@echo Usage: %0 ^<full path to ninja build directory^>
exit /B 1

:eof
```
This batch file should be invoked from the command prompt. Note that you will have to edit the batch file to enter the correct path of the Ninja binaries, and I am using a ```/``` not a ```\```. If you switch them CMake will interpret it as an escape sequence.
```
gen_compile_commands.bat <full path to Ninja build directory>
```
The batch file takes the full path to the Ninja build directory (relative paths will not work) which is the first parameter(`%1`). Once the json file is built one can use that to run clang-tidy. I happen to have many sub-directories with lots of C++ files. Therefore I opted to run clang-tidy from a batch file that would _tidy up_ all my C++ files. 

### Batch file for running clang-tidy across all C++ files in the various sub-directories
```ctall.bat
@echo off
if [%1]==[] goto usage

setlocal

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
set F=%1\cppfiles.txt

pushd %1
echo. 2>%F%

pushd ..\src
forfiles /s /m _cpp /c "cmd /c echo @path >> %F%"
popd

for /F "tokens=*" %%L in (cppfiles.txt) do clang-tidy -p .\compile_commands.json -header-filter=.* %%L

popd

del %F%

endlocal

@echo Done.
goto :eof

:usage
@echo Usage: %0 ^<full path to ninja build directory^>
exit /B 1

:eof
```
This batch file should be invoked from the command prompt or VS2015, more on that later.
```
ctall.bat <full path to Ninja build directory>
```
The batch file takes the full path to the Ninja build directory (relative paths will not work) which is the first parameter(`%1`). The output of the batch file can be redirected to another file and then one can go through each suggestion made by clang-tidy and fix it. However, that is somewhat tedious, which leads me to the next section.

### Running the batch file to tidy up from VS2015
Most VS versions support external tools, so this may apply to versions other than 2015. Choose from the menu ```Tools > External Tools... >```. This brings up a dialog box where new tools may be added. Hit the _Add_ button and enter a suitable title. For the _Command_ value enter the file name with the full path to the batch file _ctall.bat_. For _Arguments_ use the value ```$(SolutionDir)..\buildx64_ninja```. This is the path to my ninja build directory so be sure to change it for yours. The _Initial directory_ can be the same as the directory that contains _ctall.bat_. Make sure to check _Use Output window_. Then use the _OK_ button to save. You should now see a new entry in _Tools_ that has the same name as was entered in the previous dialog in the _Title_ entry field. When you choose this menu option it will run _ctall.bat_ and its output will end up in the _Output_ window in VS. When you double-click on a line in the output, VS will load the file (but alas) not bring you to the line of the error. You will have to jump to the line manually. I think this has to do with the way clang-tidy outputs file names and line numbers. 

Some of us may want to run clang-tidy on individual files from VS. In that case use the following batch file.
### Batch file for running clang-tidy on an individual C++ source file
```ctsingle.bat
@echo off
if [%1]==[] goto usage
if [%2]==[] goto usage

setlocal

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
set F=%1\cppfiles.txt

pushd %1

clang-tidy -p .\compile_commands.json -header-filter=.* %2

popd

endlocal

@echo Done.
goto :eof

:usage
@echo Usage: %0 ^<full path to ninja build directory^> ^<full path to file to tidy^>
exit /B 1

:eof
```
Choose from the menu ```Tools > External Tools... >```. This brings up a dialog box where new tools may be added. Hit the _Add_ button and enter a suitable title. For the _Command_ value enter the file name with the full path to the batch file _ctsingle.bat_. For _Arguments_ use the value ```$(SolutionDir)..\buildx64_ninja $(ItemPath)```. This is the path to my ninja build directory so be sure to change it for yours. The _Initial directory_ can be the same as the directory that contains _ctsingle.bat_. Make sure to check _Use Output window_. Then use the _OK_ button to save. You should now see a new entry in _Tools_ that has the same name as was entered in the previous dialog in the _Title_ entry field. When you choose this menu option it will run _ctsingle.bat_ on the current file in the editor window and its output will end up in the _Output_ window in VS. 
